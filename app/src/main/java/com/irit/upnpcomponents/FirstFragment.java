package com.irit.upnpcomponents;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.irit.upnpcomponents.device.UPnPListAdapter;
import com.irit.stores.AndroidUpnpServiceStore;


import org.fourthline.cling.UpnpService;

import java.util.function.Consumer;

public class FirstFragment extends Fragment {

    public static UPnPListAdapter listAdapter;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_first, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        listAdapter = new UPnPListAdapter(getContext());

        ListView lView = (ListView)getActivity().findViewById(R.id.componentList);
        lView.setAdapter(listAdapter);

        AndroidUpnpServiceStore.bindAndroidUpnpService(
                getActivity(),
                new Consumer<UpnpService>() {
                    @Override
                    public void accept(UpnpService upnpService) {
                        Toast.makeText(getActivity(), "Service bound", Toast.LENGTH_SHORT).show();
                    }

                }
        );
    }


}