package com.irit.upnpcomponents.device;

import android.view.View;

import org.fourthline.cling.model.meta.LocalDevice;

public abstract class AndroidComponent {

    private LocalDevice localDevice = null;

    private View view = null;

    public LocalDevice getLocalDevice() {
        return localDevice;
    }

    public void setLocalDevice(LocalDevice localDevice) {
        this.localDevice = localDevice;
    }

    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }

    public void onDestroy() {

    }
}
