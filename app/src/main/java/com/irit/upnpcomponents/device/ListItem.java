package com.irit.upnpcomponents.device;

import android.widget.Button;

public class ListItem {

    private Button deleteButton;

    private AndroidComponent androidComponent;

    public ListItem(AndroidComponent androidComponent){
        this.androidComponent = androidComponent;
    }

    public Button getDeleteButton() {
        return deleteButton;
    }

    public void setDeleteButton(Button deleteButton) {
        this.deleteButton = deleteButton;
    }

    public AndroidComponent getAndroidComponent() {
        return androidComponent;
    }

    public void setAndroidComponent(AndroidComponent androidComponent) {
        this.androidComponent = androidComponent;
    }
}
