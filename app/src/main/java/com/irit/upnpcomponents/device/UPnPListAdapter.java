package com.irit.upnpcomponents.device;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;

import com.irit.upnpcomponents.R;
import com.irit.upnpcomponents.components.clicker.Clicker;
import com.irit.upnpcomponents.components.lamp.Lamp;
import com.irit.upnpcomponents.components.onOffButtons.OnOffButtons;
import com.irit.upnpcomponents.components.textsender.TextSenderDevice;
import com.irit.stores.UpnpServiceStore;


import java.util.ArrayList;

public class UPnPListAdapter extends BaseAdapter implements android.widget.ListAdapter {

    private ArrayList<ListItem> list = new ArrayList<>();
    private Context context;

    private boolean deleteButtonsAreVisible = false;

    public UPnPListAdapter(Context context) {
        this.context = context;
    }
    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int pos) {
        return list.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.linear_layout, null);

        View deleteBtnView = inflater.inflate(R.layout.delete_btn_view, null);

        deleteBtnView.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        ));

        View compView = null;

        if (list.get(position).getAndroidComponent() instanceof TextSenderDevice) {
            compView = inflater.inflate(R.layout.text_sender_component, null);
        }else if(list.get(position).getAndroidComponent() instanceof Lamp){
            compView = inflater.inflate(R.layout.lamp_component,null);
        }else if(list.get(position).getAndroidComponent() instanceof Clicker) {
            compView = inflater.inflate(R.layout.clicker,null);
        }else if (list.get(position).getAndroidComponent() instanceof OnOffButtons){
            compView = inflater.inflate(R.layout.on_off_layout,null);
        }
        else {
            compView = inflater.inflate(R.layout.text_component, null);
        }

        compView.setLayoutParams(new LinearLayout.LayoutParams(
                0,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                1
        ));

        ((LinearLayout)view.findViewById(R.id.linearLayout)).addView(compView);

        ((LinearLayout)view.findViewById(R.id.linearLayout)).addView(deleteBtnView);

        list.get(position).getAndroidComponent().setView(view);

        Button deleteBtn = (Button)view.findViewById(R.id.deleteBtn2);
        list.get(position).setDeleteButton(deleteBtn);

        deleteBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(UpnpServiceStore.getUpnpService() != null){
                    UpnpServiceStore.getUpnpService().getRegistry().removeDevice(list.get(position).getAndroidComponent().getLocalDevice());
                }
                list.get(position).getAndroidComponent().onDestroy();
                list.remove(list.get(position));
                notifyDataSetChanged();
            }
        });

        if(deleteButtonsAreVisible){
            deleteBtn.setVisibility(View.VISIBLE);
        }else {
            deleteBtn.setVisibility(View.GONE);
        }

        return view;
    }

    public boolean areBtnsVisible(){
        return deleteButtonsAreVisible;
    }

    public void toggleBtnsVisibility(){
        deleteButtonsAreVisible = !deleteButtonsAreVisible;
        for(ListItem listItem : list){
            if(listItem.getDeleteButton() != null){
                if(deleteButtonsAreVisible){
                    listItem.getDeleteButton().setVisibility(View.VISIBLE);
                }else {
                    listItem.getDeleteButton().setVisibility(View.GONE);
                }

            }
        }
    }


    public void addTextSender() {
        if(UpnpServiceStore.getUpnpService() == null){
            return;
        }

        TextSenderDevice device = new TextSenderDevice();

        UpnpServiceStore.getUpnpService().getRegistry().addDevice(device.getLocalDevice());


        this.list.add(new ListItem(device));

        notifyDataSetChanged();
    }


    public void addAndroidComponent(AndroidComponent androidComponent){
        UpnpServiceStore.getUpnpService().getRegistry().addDevice(androidComponent.getLocalDevice());
        this.list.add(new ListItem(androidComponent));

        notifyDataSetChanged();
    }
}
