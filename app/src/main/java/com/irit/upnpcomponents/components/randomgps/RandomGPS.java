package com.irit.upnpcomponents.components.randomgps;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.irit.upnpcomponents.R;
import com.irit.upnpcomponents.device.AndroidComponent;
import com.irit.dependencyinjection.DependencyInjectionService;
import com.irit.factory.DeviceFactory;
import com.irit.factory.ServiceFactory;

import org.fourthline.cling.model.meta.LocalService;
import org.fourthline.cling.model.types.ServiceId;
import org.fourthline.cling.model.types.UDAServiceId;

import java.util.HashMap;
import java.util.Map;


public class RandomGPS extends AndroidComponent {

    private static LocalService<DependencyInjectionService> localService = null;

    private Thread thread;

    private boolean stop = false;
    public RandomGPS(final Activity activity) {
        Map<String, ServiceId> map = new HashMap<>();
        map.put("SetLocation", new UDAServiceId("SetLocation"));

        localService = ServiceFactory.makeDependencyInjectionService(map);

        setLocalDevice(DeviceFactory.makeLocalDevice(
                "RandomPhoneGPS",
                "Sends a random location to required service",
                1,
                "IRIT",
                new LocalService[]{
                        localService
                }
        ));

        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                double lat = 43.39605135;
                double longi = 1.72284662;

                while (!stop) {
                    try {
                        Map<String, Object> args = new HashMap<>();

                        lat += 0.05-Math.random()/10;
                        longi += 0.05-Math.random()/10;

                        if(getView() != null) {
                            double finalLat = lat;
                            double finalLongi = longi;
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    ((TextView)getView().findViewById(R.id.textView2)).setText(
                                            "Latitude : " + finalLat + "\nLongitude : " + finalLongi
                                    );
                                }
                            });
                        }

                        System.out.println(lat + " / " + longi);

                        args.put("Longitude", "" + longi);
                        args.put("Latitude", "" + lat);
                        localService.getManager().getImplementation().getRequired().get("SetLocation").execute(
                                "SetLocation",
                                args
                        );
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }

            }
        });
        thread.start();
    }

    public void setView(View view) {
        super.setView(view);
        ((TextView)view.findViewById(R.id.textView2)).setText("Random GPS");
    }

    private double randomLatitude() {
        return 90 - 180*Math.random();
    }

    private double randomLongitude() {
        return 180 - 360*Math.random();
    }

    public void onDestroy() {
        stop = true;
    }
}
