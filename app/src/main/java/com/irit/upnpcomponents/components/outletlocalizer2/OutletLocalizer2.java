package com.irit.upnpcomponents.components.outletlocalizer2;

import android.util.Pair;
import android.view.View;
import android.widget.TextView;

import com.irit.upnpcomponents.R;
import com.irit.upnpcomponents.device.AndroidComponent;
import com.irit.dependencyinjection.DependencyInjectionService;
import com.irit.factory.DeviceFactory;
import com.irit.factory.ServiceFactory;

import org.fourthline.cling.model.action.ActionInvocation;
import org.fourthline.cling.model.meta.LocalService;
import org.fourthline.cling.model.types.ServiceId;
import org.fourthline.cling.model.types.UDAServiceId;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class OutletLocalizer2 extends AndroidComponent {

    private DependencyInjectionService dependencyInjectionService = null;

    private SetLocationService setLocationService = null;

    private Thread thread;

    private boolean stop = false;

    private static final List<Pair<Double,Double>> OUTLET_POSITIONS = getOutlePositions();

    public OutletLocalizer2() {
        Map<String, ServiceId> map = new HashMap<>();
        map.put("GetBatteryLevel", new UDAServiceId("GetBatteryLevel"));
        map.put("DisplayPath", new UDAServiceId("DisplayPath"));
        map.put("DisplayPoints", new UDAServiceId("DisplayPoints"));

        LocalService<DependencyInjectionService> dependencyInjectionLocalService = ServiceFactory.makeDependencyInjectionService(map);

        dependencyInjectionService = dependencyInjectionLocalService.getManager().getImplementation();

        LocalService<SetLocationService> setLocationLocalService = ServiceFactory.makeLocalServiceFrom(SetLocationService.class);

        setLocationService = setLocationLocalService.getManager().getImplementation();

        setLocalDevice(DeviceFactory.makeLocalDevice(
                "OutletLocalizer",
                "Display Path From current location to the closest outlet",
                1,
                "IRIT",
                new LocalService[]{
                        dependencyInjectionLocalService,
                        setLocationLocalService
                }
        ));

        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while(!stop) {
                    dependencyInjectionService.getRequired().get("GetBatteryLevel").execute(
                            "GetBatteryLevel",
                            new HashMap<>(),
                            new Consumer<ActionInvocation>() {
                                @Override
                                public void accept(ActionInvocation actionInvocation) {
                                    try{
                                        int level = Integer.parseInt(actionInvocation.getOutput("Level").toString());

                                        if(level <= 100){
                                            double latitude = Double.parseDouble(setLocationService.getLatitude());
                                            double longitude = Double.parseDouble(setLocationService.getLongitude());

                                            Pair<Double,Double> closest = findClosestFrom(latitude,longitude);

                                            Map<String, Object> args = new HashMap<>();

                                            args.put("Latitude", "" + closest.first);
                                            args.put("Longitude", "" + closest.second);


                                            dependencyInjectionService.getRequired().get("DisplayPath").execute(
                                                    "DisplayPathTo",
                                                    args
                                            );
                                        }
                                    }catch (Exception e) {
                                        System.out.println(e.getMessage());
                                    }
                                }
                            }
                    );

                    Map<String, Object> args = new HashMap<>();
                    args.put("Points", getOutletPositionsToString());
                    dependencyInjectionService.getRequired().get("DisplayPoints").execute(
                            "DisplayPoints",
                            args
                    );

                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        thread.start();
    }

    private Pair<Double,Double> findClosestFrom(double latitude, double longitude) {
        Pair<Double,Double> closest = OUTLET_POSITIONS.get(0);

        for(Pair<Double,Double> pair : OUTLET_POSITIONS) {
            if(
                    Math.pow(pair.first - latitude, 2) + Math.pow(pair.second - longitude, 2) <
                            Math.pow(closest.first - latitude, 2) + Math.pow(closest.second - longitude, 2)
            ){
                closest = pair;
            }
        }
        return closest;
    }

    public void setView(View view) {
        super.setView(view);
        ((TextView)view.findViewById(R.id.textView2)).setText("OutletLocalizer2");
    }

    @Override
    public void onDestroy() {
        stop = true;
    }

    private static List<Pair<Double,Double>> getOutlePositions() {
        List<Pair<Double,Double>> list = new ArrayList<>();

        double centerLat = 43.39605135;
        double centerLong = 1.72284662;

        for(int i = 0; i < 50; i++) {
            Pair<Double,Double> newPair = new Pair<>(
                    centerLat + 0.7 - 1.4*Math.random(),
                    centerLong + 0.7 - 1.4*Math.random()
            );
            list.add(newPair);
        }

        return list;
    }

    private static String getOutletPositionsToString() {
        StringBuilder returned = new StringBuilder();

        for(Pair<Double,Double> outlet : OUTLET_POSITIONS) {
            returned.append(outlet.first).append(",").append(outlet.second).append(" ");
        }
        returned.deleteCharAt(returned.length()-1);
        return returned.toString();
    }
}
