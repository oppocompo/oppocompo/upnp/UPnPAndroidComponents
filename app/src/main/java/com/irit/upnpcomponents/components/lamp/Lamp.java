package com.irit.upnpcomponents.components.lamp;

import android.app.Activity;

import android.view.View;
import android.widget.ImageView;

import com.irit.factory.DeviceFactory;
import com.irit.factory.ServiceFactory;
import com.irit.upnpcomponents.R;
import com.irit.upnpcomponents.device.AndroidComponent;
import org.fourthline.cling.model.meta.LocalDevice;
import org.fourthline.cling.model.meta.LocalService;


public class Lamp extends AndroidComponent {

    private LampSetterService lampSetterService;
    private LampTogglerService lampTogglerService;

    private Activity activity;

    private boolean isOn;

    public Lamp(Activity activity) {
        this.activity = activity;
        setLocalDevice(createDevice());
    }

    private LocalDevice createDevice() {

        LocalService<LampSetterService> localServiceSetter = setLampSetterListener();
        LocalService<LampTogglerService> localServiceToggler = setLampTogglerListener();

        return DeviceFactory.makeLocalDevice(
                "AndroidLamp",
                "a Lamp",
                1,
                "IRIT",
                new LocalService[]{
                        localServiceSetter,localServiceToggler
                }
        );
    }

    public void setView(View view) {
        super.setView(view);

        if (isOn){
            ((ImageView)view.findViewById(R.id.ampoule)).setImageResource(R.drawable.ampoule_on);
        }else{
            ((ImageView)view.findViewById(R.id.ampoule)).setImageResource(R.drawable.ampoule_off);
        }
    }


    private void syncLampServices(ILamp updatedLamp, ILamp lampToSync){
        lampToSync.setStatus(updatedLamp.getStatus());
    }

    private LocalService<LampTogglerService> setLampTogglerListener(){
        LocalService<LampTogglerService> localServiceToggler = ServiceFactory.makeLocalServiceFrom(LampTogglerService.class);
        lampTogglerService = localServiceToggler.getManager().getImplementation();
        localServiceToggler.getManager().getImplementation().getPropertyChangeSupport().addPropertyChangeListener("isOn", propertyChangeEvent -> {
            if(Lamp.this.getView() != null){
                activity.runOnUiThread(() -> {
                    isOn = (Boolean)propertyChangeEvent.getNewValue();
                    syncLampServices(lampTogglerService,lampSetterService);
                    setView(Lamp.this.getView());
                });
            }
        });
        return localServiceToggler;
    }

    private LocalService<LampSetterService> setLampSetterListener(){
        LocalService<LampSetterService> localServiceSetter = ServiceFactory.makeLocalServiceFrom(LampSetterService.class);
        lampSetterService = localServiceSetter.getManager().getImplementation();
        localServiceSetter.getManager().getImplementation().getPropertyChangeSupport().addPropertyChangeListener("isOn", propertyChangeEvent -> {
            if(Lamp.this.getView() != null){
                activity.runOnUiThread(() -> {
                    isOn = (Boolean)propertyChangeEvent.getNewValue();
                    syncLampServices(lampSetterService,lampTogglerService);
                    setView(Lamp.this.getView());
                });
            }
        });
        return localServiceSetter;
    }
}
