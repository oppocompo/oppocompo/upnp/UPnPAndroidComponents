package com.irit.upnpcomponents.components.textsender;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.irit.upnpcomponents.R;
import com.irit.upnpcomponents.device.AndroidComponent;

import com.irit.dependencyinjection.DependencyInjectionService;
import com.irit.factory.DeviceFactory;
import com.irit.factory.ServiceFactory;

import org.fourthline.cling.model.meta.LocalService;
import org.fourthline.cling.model.types.ServiceId;
import org.fourthline.cling.model.types.UDAServiceId;

import java.util.HashMap;
import java.util.Map;

public class TextSenderDevice extends AndroidComponent {

    private LocalService<DependencyInjectionService> localService;

    private String lastWrittenText = "Some Text";

    public TextSenderDevice() {
        Map<String, ServiceId> map = new HashMap<>();
        map.put("SetText", new UDAServiceId("SetText"));

        localService = ServiceFactory.makeDependencyInjectionService(map);

        setLocalDevice(DeviceFactory.makeLocalDevice(
                "AndroidTextSender",
                "Sends text input to a required service",
                1,
                "IRIT",
                new LocalService[]{localService}
        ));
    }

    public void setView(View view){
        super.setView(view);

        if(view == null){
            return;
        }

        Button button = (Button) view.findViewById(R.id.sendButton);
        EditText editText = (EditText) view.findViewById(R.id.editTextToBeSent);
        editText.setText(lastWrittenText);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                lastWrittenText = charSequence.toString();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Map<String,Object> args = new HashMap<>();
                args.put("Text", editText.getText().toString());

                localService.getManager().getImplementation().getRequired().get("SetText").execute(
                        "SetText",
                        args
                );
            }
        });
    }
}
