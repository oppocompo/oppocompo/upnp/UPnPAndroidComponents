package com.irit.upnpcomponents.components.batterylevelindicator;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.irit.upnpcomponents.R;
import com.irit.upnpcomponents.device.AndroidComponent;
import com.irit.factory.DeviceFactory;
import com.irit.factory.ServiceFactory;

import org.fourthline.cling.model.meta.LocalService;

public class BatteryLevelIndicator extends AndroidComponent {

    public BatteryLevelIndicator(Context context) {

        LocalService<GetBatteryLevelService> batteryLevelService = ServiceFactory.makeLocalServiceFrom(GetBatteryLevelService.class);

        batteryLevelService.getManager().getImplementation().setContext(context);

        setLocalDevice(DeviceFactory.makeLocalDevice(
                "PhoneBatteryLevel",
                "Transmit the battery's level and state in a pull manner",
                1,
                "IRIT",
                new LocalService[]{batteryLevelService}
        ));
    }

    public void setView(View view){
        super.setView(view);

        if(view == null){
            return;
        }

        TextView textView = (TextView) view.findViewById(R.id.textView2);
        textView.setText("BatteryLevelIndicator");
    }
}
