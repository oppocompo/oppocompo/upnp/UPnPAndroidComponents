package com.irit.upnpcomponents.components.batterylevelindicator;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;

import org.fourthline.cling.binding.annotations.UpnpAction;
import org.fourthline.cling.binding.annotations.UpnpOutputArgument;
import org.fourthline.cling.binding.annotations.UpnpService;
import org.fourthline.cling.binding.annotations.UpnpServiceId;
import org.fourthline.cling.binding.annotations.UpnpServiceType;
import org.fourthline.cling.binding.annotations.UpnpStateVariable;

@UpnpService(
        serviceId = @UpnpServiceId("GetBatteryLevel"),
        serviceType = @UpnpServiceType(value = "GetBatteryLevel", version = 1)
)
public class GetBatteryLevelService {

    private Context context;

    Intent batteryStatus;

    @UpnpStateVariable
    private int level;

    @UpnpStateVariable
    private String state;

    @UpnpAction(name = "GetBatteryLevel", out = @UpnpOutputArgument(name = "Level"))
    public int getBatteryLevel(){
        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

        this.level = Math.floorDiv(level * 100, scale);

        return this.level;
    }

    @UpnpAction(name = "GetBatteryState", out = @UpnpOutputArgument(name = "State"))
    public String getBatteryState(){
        int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);

        boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                status == BatteryManager.BATTERY_STATUS_FULL;

        state = isCharging?"CHARGING":"NOTCHARGING";

        return state;
    }

    public void setContext(Context context) {
        this.context = context;

        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        batteryStatus = context.registerReceiver(null, ifilter);
    }
}
