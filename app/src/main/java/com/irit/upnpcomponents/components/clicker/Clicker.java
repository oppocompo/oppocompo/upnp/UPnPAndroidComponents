package com.irit.upnpcomponents.components.clicker;

import android.view.View;
import android.widget.Button;
import com.irit.dependencyinjection.DependencyInjectionService;
import com.irit.factory.DeviceFactory;
import com.irit.factory.ServiceFactory;
import com.irit.upnpcomponents.R;
import com.irit.upnpcomponents.device.AndroidComponent;
import org.fourthline.cling.model.meta.LocalService;
import org.fourthline.cling.model.types.ServiceId;
import org.fourthline.cling.model.types.UDAServiceId;
import java.util.HashMap;
import java.util.Map;

public class Clicker extends AndroidComponent {

    private LocalService<DependencyInjectionService> localService;

    public Clicker() {
        Map<String, ServiceId> map = new HashMap<>();
        map.put("Toggle", new UDAServiceId("Toggle"));

        localService = ServiceFactory.makeDependencyInjectionService(map);

        setLocalDevice(DeviceFactory.makeLocalDevice(
                "AndroidClicker",
                "A standard button",
                1,
                "IRIT",
                new LocalService[]{localService}
        ));
    }

    public void setView(View view) {
        super.setView(view);
        if (view == null) {
            return;
        }
        Button button = (Button) view.findViewById(R.id.button);

        button.setOnClickListener(v -> {
            Map<String,Object> args = new HashMap<>();
            localService.getManager().getImplementation().getRequired().get("Toggle").execute("Toggle",args);
        });
    }
}
