package com.irit.upnpcomponents.components.lamp;

import org.fourthline.cling.binding.annotations.UpnpAction;
import org.fourthline.cling.binding.annotations.UpnpInputArgument;
import org.fourthline.cling.binding.annotations.UpnpService;
import org.fourthline.cling.binding.annotations.UpnpServiceId;
import org.fourthline.cling.binding.annotations.UpnpServiceType;
import org.fourthline.cling.binding.annotations.UpnpStateVariable;

import java.beans.PropertyChangeSupport;

@UpnpService(
        serviceId = @UpnpServiceId("SetOn"),
        serviceType = @UpnpServiceType(value = "SetOn",version = 1)
)
public class LampSetterService implements ILamp{

    @UpnpStateVariable
    private boolean isOn = false;

    private PropertyChangeSupport propertyChangeSupport;

    public LampSetterService(){
        propertyChangeSupport = new PropertyChangeSupport(this);
        propertyChangeSupport.firePropertyChange("isOn",isOn,false);
    }

    @UpnpAction(name = "SetOn")
    public Boolean setOn(@UpnpInputArgument(name = "IsOn") Boolean setter){
        propertyChangeSupport.firePropertyChange("isOn", java.util.Optional.of(isOn),setter);
        isOn = setter;
        return isOn;
    }

    public PropertyChangeSupport getPropertyChangeSupport() { return propertyChangeSupport; }

    @Override
    public Boolean getStatus() {
        return this.isOn;
    }

    @Override
    public void setStatus(Boolean b) {
        this.isOn = b;
    }
}
