package com.irit.upnpcomponents.components.gpstodisplaypathconverter;

import android.view.View;
import android.widget.TextView;

import com.irit.upnpcomponents.R;
import com.irit.upnpcomponents.device.AndroidComponent;
import com.irit.dependencyinjection.DependencyInjectionService;
import com.irit.factory.DeviceFactory;
import com.irit.factory.ServiceFactory;

import org.fourthline.cling.model.meta.LocalService;
import org.fourthline.cling.model.types.ServiceId;
import org.fourthline.cling.model.types.UDAServiceId;

import java.util.HashMap;
import java.util.Map;

public class SetLocationToDisplayPathConverter extends AndroidComponent {

    public SetLocationToDisplayPathConverter() {
        Map<String, ServiceId> map = new HashMap<>();
        map.put("DisplayPath", new UDAServiceId("DisplayPath"));

        LocalService<DependencyInjectionService> dependencyInjectionLocalService = ServiceFactory.makeDependencyInjectionService(map);

        LocalService<LocationToPathService>  setLocationLocalService = ServiceFactory.makeLocalServiceFrom(LocationToPathService.class);

        setLocationLocalService.getManager().getImplementation().setDependencyInjectionService(dependencyInjectionLocalService.getManager().getImplementation());

        setLocalDevice(DeviceFactory.makeLocalDevice(
                "LocationToPathAdapter",
                "SetLocation to DisplayPath Converter",
                1,
                "IRIT",
                new LocalService[]{
                        dependencyInjectionLocalService,
                        setLocationLocalService
                }
        ));
    }



    public void setView(View view) {
        super.setView(view);
        ((TextView)view.findViewById(R.id.textView2)).setText("Location To Path Converter");
    }
}
