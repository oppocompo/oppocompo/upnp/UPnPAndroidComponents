package com.irit.upnpcomponents.components.lamp;

public interface ILamp {

    public Boolean getStatus();
    public void setStatus(Boolean b);
}
