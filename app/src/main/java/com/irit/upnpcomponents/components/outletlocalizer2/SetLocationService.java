package com.irit.upnpcomponents.components.outletlocalizer2;

import org.fourthline.cling.binding.annotations.UpnpAction;
import org.fourthline.cling.binding.annotations.UpnpInputArgument;
import org.fourthline.cling.binding.annotations.UpnpService;
import org.fourthline.cling.binding.annotations.UpnpServiceId;
import org.fourthline.cling.binding.annotations.UpnpServiceType;
import org.fourthline.cling.binding.annotations.UpnpStateVariable;

@UpnpService(
        serviceId = @UpnpServiceId("SetLocation"),
        serviceType = @UpnpServiceType(value = "SetLocation", version = 1)
)
public class SetLocationService {

    @UpnpStateVariable
    private String longitude;

    @UpnpStateVariable
    private String latitude;

    @UpnpAction(name = "SetLocation")
    public void setLocation(
            @UpnpInputArgument(name = "Longitude") String longitude,
            @UpnpInputArgument(name = "Latitude") String latitude
    ){
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getLatitude() {
        return latitude;
    }
}
