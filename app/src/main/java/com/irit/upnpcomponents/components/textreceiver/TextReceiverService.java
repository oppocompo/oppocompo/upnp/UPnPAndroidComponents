package com.irit.upnpcomponents.components.textreceiver;


import org.fourthline.cling.binding.annotations.*;

import java.beans.PropertyChangeSupport;

@UpnpService(
        serviceId = @UpnpServiceId("SetText"),
        serviceType = @UpnpServiceType(value = "SetText", version = 1)
)
public class TextReceiverService {

    private int callCount = 0;

    @UpnpStateVariable
    private String text = null;

    private PropertyChangeSupport propertyChangeSupport ;

    public TextReceiverService(){
        propertyChangeSupport = new PropertyChangeSupport(this);
        propertyChangeSupport.firePropertyChange("text","old","text");
    }

    @UpnpAction(name = "SetText", out = @UpnpOutputArgument(name = "Ret"))
    public String setText(@UpnpInputArgument(name = "Text") String text){
        this.text = text;
        //System.out.println("AAAA " + text);
        propertyChangeSupport.firePropertyChange("text","old","#" + callCount + " : " + text);
        callCount++;
        return "ok";
    }

    public PropertyChangeSupport getPropertyChangeSupport() {
        return propertyChangeSupport;
    }

    public String getText() {
        return text;
    }
}
