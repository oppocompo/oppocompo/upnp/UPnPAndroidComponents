package com.irit.upnpcomponents.components.lamp;

import org.fourthline.cling.binding.annotations.UpnpAction;
import org.fourthline.cling.binding.annotations.UpnpOutputArgument;
import org.fourthline.cling.binding.annotations.UpnpService;
import org.fourthline.cling.binding.annotations.UpnpServiceId;
import org.fourthline.cling.binding.annotations.UpnpServiceType;
import org.fourthline.cling.binding.annotations.UpnpStateVariable;

import java.beans.PropertyChangeSupport;

@UpnpService(
        serviceId = @UpnpServiceId("Toggle"),
        serviceType = @UpnpServiceType(value = "Toggle",version = 1)
)
public class LampTogglerService implements ILamp {

    @UpnpStateVariable
    private boolean isOn = false;

    private PropertyChangeSupport propertyChangeSupport;

    public LampTogglerService(){
        propertyChangeSupport = new PropertyChangeSupport(this);
        propertyChangeSupport.firePropertyChange("isOn",isOn,false);
    }

    @UpnpAction(name = "Toggle", out = @UpnpOutputArgument(name = "IsOn"))
    public Boolean toggle() {
        propertyChangeSupport.firePropertyChange("isOn",isOn,!isOn);
        isOn = !isOn;
        return this.isOn;
    }

    public boolean isOn() {
        return isOn;
    }

    public PropertyChangeSupport getPropertyChangeSupport() {
        return propertyChangeSupport;
    }

    @Override
    public Boolean getStatus() {
        return this.isOn;
    }

    @Override
    public void setStatus(Boolean b) {
        this.isOn = b;
    }
}
