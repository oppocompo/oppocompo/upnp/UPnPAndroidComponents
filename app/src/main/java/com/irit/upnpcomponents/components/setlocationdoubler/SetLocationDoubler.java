package com.irit.upnpcomponents.components.setlocationdoubler;

import android.view.View;
import android.widget.TextView;

import com.irit.upnpcomponents.R;
import com.irit.upnpcomponents.device.AndroidComponent;
import com.irit.dependencyinjection.DependencyInjectionService;
import com.irit.factory.DeviceFactory;
import com.irit.factory.ServiceFactory;

import org.fourthline.cling.model.meta.LocalService;
import org.fourthline.cling.model.types.ServiceId;
import org.fourthline.cling.model.types.UDAServiceId;

import java.util.HashMap;
import java.util.Map;

public class SetLocationDoubler extends AndroidComponent {

    public SetLocationDoubler() {

        Map<String, ServiceId> map = new HashMap<>();
        map.put("SetLocation1", new UDAServiceId("SetLocation"));
        map.put("SetLocation2", new UDAServiceId("SetLocation"));

        LocalService<DependencyInjectionService> dependencyInjectionLocalService = ServiceFactory.makeDependencyInjectionService(map);

        LocalService<SetLocationService>  setLocationLocalService = ServiceFactory.makeLocalServiceFrom(SetLocationService.class);

        setLocationLocalService.getManager().getImplementation().setDependencyInjectionService(dependencyInjectionLocalService.getManager().getImplementation());

        setLocalDevice(DeviceFactory.makeLocalDevice(
                "GPSDoubler",
                "Sends a received Location to two required services",
                1,
                "IRIT",
                new LocalService[]{
                        dependencyInjectionLocalService,
                        setLocationLocalService
                }
        ));

    }

    public void setView(View view) {
        super.setView(view);
        ((TextView)view.findViewById(R.id.textView2)).setText("GPS Doubler");
    }
}
