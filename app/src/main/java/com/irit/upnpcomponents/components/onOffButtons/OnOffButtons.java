package com.irit.upnpcomponents.components.onOffButtons;

import android.view.View;
import android.widget.Button;

import com.irit.dependencyinjection.DependencyInjectionService;
import com.irit.factory.DeviceFactory;
import com.irit.factory.ServiceFactory;
import com.irit.upnpcomponents.R;
import com.irit.upnpcomponents.device.AndroidComponent;

import org.fourthline.cling.model.meta.LocalService;
import org.fourthline.cling.model.types.ServiceId;
import org.fourthline.cling.model.types.UDAServiceId;

import java.util.HashMap;
import java.util.Map;

public class OnOffButtons extends AndroidComponent {

    private LocalService<DependencyInjectionService> localService;

    public OnOffButtons(){
        Map<String, ServiceId> map = new HashMap<>();
        map.put("SetOn",new UDAServiceId("SetOn"));

        localService = ServiceFactory.makeDependencyInjectionService(map);

        setLocalDevice(DeviceFactory.makeLocalDevice(
                "AndroidOnOff",
                "Two buttons used to turn on or off something",
                1,
                "IRIT",
                new LocalService[]{localService}
        ));
    }

    public void setView(View view) {
        super.setView(view);
        if (view == null) {
            return;
        }
        Button on = (Button) view.findViewById(R.id.on_button);
        Button off = (Button) view.findViewById(R.id.off_button);

        on.setOnClickListener(v -> {
            updateComponent(true);
        });

        off.setOnClickListener(v -> {
            updateComponent(false);
        });
    }

    private void updateComponent(Boolean isOn){
        Map<String,Object> args = new HashMap<>();
        args.put("IsOn",isOn);
        localService.getManager().getImplementation().getRequired().get("SetOn").execute("SetOn",args);
    }
}
