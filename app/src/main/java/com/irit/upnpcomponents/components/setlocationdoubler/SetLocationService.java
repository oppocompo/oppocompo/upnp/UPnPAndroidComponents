package com.irit.upnpcomponents.components.setlocationdoubler;

import com.irit.dependencyinjection.DependencyInjectionService;

import org.fourthline.cling.binding.annotations.UpnpAction;
import org.fourthline.cling.binding.annotations.UpnpInputArgument;
import org.fourthline.cling.binding.annotations.UpnpService;
import org.fourthline.cling.binding.annotations.UpnpServiceId;
import org.fourthline.cling.binding.annotations.UpnpServiceType;
import org.fourthline.cling.binding.annotations.UpnpStateVariable;

import java.util.HashMap;
import java.util.Map;

@UpnpService(
        serviceId = @UpnpServiceId("SetLocation"),
        serviceType = @UpnpServiceType(value = "SetLocation", version = 1)
)
public class SetLocationService {

    private DependencyInjectionService dependencyInjectionService;

    @UpnpStateVariable
    private String longitude;

    @UpnpStateVariable
    private String latitude;

    @UpnpAction(name = "SetLocation")
    public void setLocation(
            @UpnpInputArgument(name = "Longitude") String longitude,
            @UpnpInputArgument(name = "Latitude") String latitude
    ){
        this.latitude = latitude;
        this.longitude = longitude;

        Map<String, Object> args = new HashMap<>();

        args.put("Longitude", "" + longitude);
        args.put("Latitude", "" + latitude);

        dependencyInjectionService.getRequired().get("SetLocation1").execute(
                "SetLocation",
                args
        );

        dependencyInjectionService.getRequired().get("SetLocation2").execute(
                "SetLocation",
                args
        );
    }

    public void setDependencyInjectionService(DependencyInjectionService dependencyInjectionService) {
        this.dependencyInjectionService = dependencyInjectionService;
    }
}
