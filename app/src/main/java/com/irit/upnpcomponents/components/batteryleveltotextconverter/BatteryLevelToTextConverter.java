package com.irit.upnpcomponents.components.batteryleveltotextconverter;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.irit.upnpcomponents.R;
import com.irit.upnpcomponents.device.AndroidComponent;
import com.irit.dependencyinjection.DependencyInjectionService;
import com.irit.factory.DeviceFactory;
import com.irit.factory.ServiceFactory;

import org.fourthline.cling.model.action.ActionInvocation;
import org.fourthline.cling.model.meta.LocalService;
import org.fourthline.cling.model.types.ServiceId;
import org.fourthline.cling.model.types.UDAServiceId;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

public class BatteryLevelToTextConverter extends AndroidComponent {

    private LocalService<DependencyInjectionService> localService = null;

    private Thread thread;

    private boolean stop = false;

    private Activity activity;

    private int nbCalls = 0;

    public BatteryLevelToTextConverter(Activity activity) {
        this.activity = activity;
        Map<String, ServiceId> map = new HashMap<>();
        map.put("GetBatteryLevel", new UDAServiceId("GetBatteryLevel"));
        map.put("SetText", new UDAServiceId("SetText"));

        localService = ServiceFactory.makeDependencyInjectionService(map);


        setLocalDevice(DeviceFactory.makeLocalDevice(
                "BatteryLevelToTextConverter",
                "Retrieves Battery Level and sends it to a text service",
                1,
                "IRIT",
                new LocalService[]{
                        localService
                }
        ));

        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (!stop) {
                    try {

                        localService.getManager().getImplementation().getRequired().get("GetBatteryLevel").execute(
                                "GetBatteryLevel",
                                new HashMap<>(),
                                new Consumer<ActionInvocation>() {
                                    @Override
                                    public void accept(ActionInvocation actionInvocation) {
                                        Map<String,Object> args = new HashMap<>();
                                        args.put("Text", "Battery at " + actionInvocation.getOutput("Level").toString() + "%");

                                        activity.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                if(getView() != null) {
                                                    ((TextView)getView().findViewById(R.id.textView2)).setText("Battery Level To Text : #" + nbCalls + " o");
                                                }
                                            }
                                        });

                                        localService.getManager().getImplementation().getRequired().get("SetText").execute(
                                                "SetText",
                                                args,
                                                new Consumer<ActionInvocation>() {
                                                    @Override
                                                    public void accept(ActionInvocation actionInvocation) {
                                                        activity.runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                if(getView() != null) {
                                                                    ((TextView)getView().findViewById(R.id.textView2)).setText("Battery Level To Text : #" + nbCalls + " ok");
                                                                }
                                                            }
                                                        });
                                                    }
                                                }
                                        );
                                        nbCalls++;
                                    }
                                }
                        );

                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        thread.start();
    }

    public void setView(View view) {
        super.setView(view);
        ((TextView)view.findViewById(R.id.textView2)).setText("Battery Level To Text");
    }

    @Override
    public void onDestroy() {
        stop = true;
    }
}
