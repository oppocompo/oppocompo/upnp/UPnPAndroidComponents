package com.irit.upnpcomponents.components.textreceiver;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.irit.upnpcomponents.R;
import com.irit.upnpcomponents.device.AndroidComponent;
import com.irit.factory.DeviceFactory;
import com.irit.factory.ServiceFactory;

import org.fourthline.cling.model.meta.*;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class TextReceiver extends AndroidComponent {

    private TextReceiverService textReceiverService;

    private Activity activity;

    public TextReceiver(Activity activity) {
        this.activity = activity;
        setLocalDevice(createDevice());
    }

    private LocalDevice createDevice() {
        LocalService<TextReceiverService>  localService = ServiceFactory.makeLocalServiceFrom(TextReceiverService.class);

        localService.getManager().getImplementation().getPropertyChangeSupport().addPropertyChangeListener("text", new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if(TextReceiver.this.getView() != null){
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ((TextView) TextReceiver.this.getView().findViewById(R.id.textView2)).setText(propertyChangeEvent.getNewValue().toString());
                        }
                    });
                }
            }
        });

        textReceiverService = localService.getManager().getImplementation();

        return DeviceFactory.makeLocalDevice(
                "AndroidTextReceiver",
                "Receives text",
                1,
                "IRIT",
                new LocalService[]{localService}
        );
    }

    public void SetView(View view) {
        super.setView(view);

        if(textReceiverService.getText() != null){
            ((TextView)view.findViewById(R.id.textView2)).setText(textReceiverService.getText());
        }
    }
}
