package com.irit.upnpcomponents;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;

import android.view.Menu;
import android.view.MenuItem;

import com.irit.upnpcomponents.components.batterylevelindicator.BatteryLevelIndicator;
import com.irit.upnpcomponents.components.batteryleveltotextconverter.BatteryLevelToTextConverter;
import com.irit.upnpcomponents.components.clicker.Clicker;
import com.irit.upnpcomponents.components.gpstodisplaypathconverter.SetLocationToDisplayPathConverter;
import com.irit.upnpcomponents.components.lamp.Lamp;
import com.irit.upnpcomponents.components.outletlocalizer2.OutletLocalizer2;
import com.irit.upnpcomponents.components.randomgps.RandomGPS;
import com.irit.upnpcomponents.components.setlocationdoubler.SetLocationDoubler;
import com.irit.upnpcomponents.components.onOffButtons.OnOffButtons;
import com.irit.upnpcomponents.components.textreceiver.TextReceiver;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);



        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.toggleDeleteBtns) {
            FirstFragment.listAdapter.toggleBtnsVisibility();
            if (FirstFragment.listAdapter.areBtnsVisible()) {
                item.setTitle("Hide delete buttons");
            } else {
                item.setTitle("Show delete buttons");
            }

            return true;
        }

        if (id == R.id.addTextReceiver) {
                FirstFragment.listAdapter.addAndroidComponent(new TextReceiver(this));

        } else if (id == R.id.addTextSender) {
                FirstFragment.listAdapter.addTextSender();

        } else if (id == R.id.addBatteryLevelIndicator) {

                BatteryLevelIndicator batteryLevelIndicator = new BatteryLevelIndicator(this.getApplicationContext());
                FirstFragment.listAdapter.addAndroidComponent(batteryLevelIndicator);

        } else if (id == R.id.addRandomGPS) {
                FirstFragment.listAdapter.addAndroidComponent(new RandomGPS(this));

        } else if (id == R.id.addSetLocationDoubler) {
                FirstFragment.listAdapter.addAndroidComponent(new SetLocationDoubler());

        } else if (id == R.id.addLocationToPathConverter) {
                FirstFragment.listAdapter.addAndroidComponent(new SetLocationToDisplayPathConverter());

        } else if (id == R.id.addBatteryToTextConverter) {
                FirstFragment.listAdapter.addAndroidComponent(new BatteryLevelToTextConverter(this));

        } else if (id == R.id.addOutletLocalizer2) {
                FirstFragment.listAdapter.addAndroidComponent(new OutletLocalizer2());

        } else if (id == R.id.test) {
            FirstFragment.listAdapter.addAndroidComponent(new Lamp(this));
        } else if (id == R.id.clicker) {
            FirstFragment.listAdapter.addAndroidComponent(new Clicker());
        } else if (id == R.id.onOff){
            FirstFragment.listAdapter.addAndroidComponent(new OnOffButtons());
        }


        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
        item.setActionView(new View(this));
        item.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return false;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                return false;
            }
        });

        return false;
    }
}