# UPnP Android Components

This project regroups several UPnP components to be used with [OCE](https://gitlab.irit.fr/oppocompo/oppocompo/OCE)

![component](img/app.png)

## Installation - Just running components

Android 7.0 is required to run the app.

[Download the apk file](upnpcomponents.apk) and install it on your device. Check ok to the unverified developer warning and launch the app.

To add a component, use the top right menu and select a component to add.

To remove a component, select *Show Delete Buttons* in the top right menu, then tap the red button next to the component you want to remove.

## Installation - Project set up

In Android Studio, create a new *Project from Version Control* and check out this repository.

Running the app on a compiler has not been tested and will probably not work as intended.

## Component details

This section regroup the description of all the components available in this app.

### Text Receiver

![component](img/textreceiver.png)

When the `SetText` action is called, display the content of the `Text` parameter.

### Text Sender

![component](img/textsender.png)

On push of the `Send` button, send the text written by the user to a bound component through its `SetText` required service.

### Battery Level Indicator

![component](img/batterylevel.png)

Informs on the state of the device's battery :
- When the `GetBatteryLevel` action of its provided service is called, transmits an integer between 0 and 100 (the device's actual charge level) as the `Level` return value.
- When the `GetBatteryState` action of its provided service is called, returns a String representing the state of the battery as the `State` return value. Can be either `CHARGING` or `NOTCHARGING`.


### Battery To text Converter

![component](img/batterytotext.png)

Every 5 seconds, retrieves the battery level through the `GetBatteryLevel` of its `GetBatteryLevel` required service, and transmits it in a descriptive String to its `SetText` required service.

### Random GPS

![component](img/randomgps.png)

Every 5 seconds, sends random coordinates in the Toulouse area to its `SetLocation` required service.

### GPS Doubler

![component](img/gpsdoubler.png)

When its `SetLocation` provided service is called, sends the received gps coordinates to its `SetLocation1` and `SetLocation2` required services.

### Location To Path Adapter

![component](img/locationtopath.png)

When its `SetLocation` provided service is called, calls its `DisplayPath` required service using the provided gps coordinates.

### OutletLocalizer2

![component](img/outletlocalizer.png)

On creation, generates a set of 50 random coordinates in the Toulouse area.

When its `SetLocation` provided service is called, calls its `GetBatteryLevel` required service, also calls its `DisplayPoints` required service with the coordinates of the 50 points as a parameter.

If the battery level is low enough (For testing purposes, low enough is <= 100), calls its `DisplayPath` required service, with the coordinates of the closest of the 5O points to the received point as a parameter.
